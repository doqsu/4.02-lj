import React, { Component } from 'react';
import { View, StyleSheet, Text, Image , Button, Modal} from 'react-native';

import userpic from '../assets/userpic.png'
import userhead from '../assets/userhead.png'


class Userbar extends Component {


  render() {
    const { 
        author
    } = this.props;

    return (
      <View style={{flexDirection: "row", alignItems:'center'}}>
          <Image 
          source={userpic} 
          style={{margin: 20, width: 75, height: 75, borderRadius: 50}}
          />
          
            <View style={{flexDirection:'row', alignItems: 'center', borderRadius: 50, backgroundColor:'#cfcfcf', paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5}}>
                <Image
                source={userhead}
                style={{width:20, height: 20}}
                />
                <Text style={{fontSize: 18, marginLeft: 10}}>{author}</Text>
            </View>

      </View>
    
    );
  }
}

const styles = StyleSheet.create({

  });
  
export default Userbar;
