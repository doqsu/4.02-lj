import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Post from './Post'

class Posts extends Component {

  render() {
    return (
      <View>
        {this.props.posts.map((item, id) => (
              <View key={id}>
                <Post 
                author={item.author}
                title={item.title}
                text={item.text}
                postDate={item.postDate}
                postTime={item.postTime}
                />
              </View>
          ))}

      </View>
    );
  }
}

export default Posts;
