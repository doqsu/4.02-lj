import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity , Button, Modal} from 'react-native';

import Userbar from './Userbar';

class Post extends Component {


  render() {
    const { 
        author, text, title, postDate, postTime
    } = this.props;

    return (
      <View>
        <Userbar author={author} postDate={postDate} postTime={postTime}/>
        <View></View>
        <Text style={{
            marginLeft: 15, 
            marginTop: 20, 
            marginRight: 15, 
            borderBottomWidth: 1, 
            borderBottomColor:'#888888',
            fontSize: 20,
            color: '#555555'
            }}>{title}</Text>
        <Text style={{
            marginLeft: 15, 
            marginTop: 5, 
            marginRight: 15, 
            marginBottom: 20,
            fontSize: 18,
            color: '#555555'
            
            }}>{text}</Text>
      </View>

    );
  }
}

const styles = StyleSheet.create({

  });


  
  export default Post;
