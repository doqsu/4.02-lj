import React, { Component } from 'react';
import { View, StyleSheet, Text, Image , Button, Modal} from 'react-native';

import userpic from '../assets/userpic.png'
import userhead from '../assets/userhead.png'


class Userbar extends Component {


  render() {
    const { 
        author, postDate, postTime
    } = this.props;

    return (
      <View style={{flexDirection: "row", alignItems:'center', backgroundColor: '#efefef'}}>
          <Image 
          source={userpic} 
          style={{margin: 15, width: 50, height: 50, borderRadius: 25}}
          />
          <View
          style={{flexDirection: 'column', marginLeft: 5, margin: 15, height: 50,}}>
            <View style={{flexDirection:'row', alignItems: 'center'}}>
                <Image
                source={userhead}
                style={{width:20, height: 20}}
                />
                <Text style={{fontSize: 18, marginLeft: 10}}>{author.toUpperCase()}</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Text style={{fontSize: 18}}>{postDate}</Text>
                <Text style={{fontSize: 18}}>{postTime}</Text>
            </View>
          </View>

      </View>
    
    );
  }
}

const styles = StyleSheet.create({

  });
  
export default Userbar;
