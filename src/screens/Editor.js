import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, AsyncStorage, FlatList } from 'react-native';
import UserSign from '../components/UserSign';

export default class Editor extends Component {
  state= {
    posts: [],
    text: '',
    author: 'username',
    title: '',
    id: '',
  };


  changeTitle(value){
    this.setState({ title: value })
    
  };

  changeText(value){
    this.setState({ text: value })
  }

  calculateDate = () => {

    let global = new Date()
  
    let day = global.getDate();
    for ( let i = 0; i < 10; i++){
        if (day == i) day = '0' + i;
    }
        let month = global.getMonth() + 1;
        switch(month){
            case 1: month = 'Jan';
            break;
            case 2: month = 'Feb';
            break;
            case 3: month = 'Mar';
            break;
            case 4: month = 'Apr';
            break;
            case 5: month = 'May';
            break;
            case 6: month = 'Jun';
            break;
            case 7: month = 'Jul';
            break;
            case 8: month = 'Aug';
            break;
            case 9: month = 'Sep';
            break;
            case 10: month = 'Oct';
            break;
            case 11: month = 'Nov';
            break;
            case 12: month = 'Dec';
            break;
        }
  
        let year = global.getFullYear().toString();
  
        let hours = global.getHours();
        for ( let i = 0; i < 10; i++){
            if (hours == i) hours = '0' + i;
        }
        let minutes = global.getMinutes();
        for ( let i = 0; i < 10; i++){
            if (minutes == i) minutes = '0' + i;
        }
        return month + '-' + day + '-' + year;

    }

    calculateTime = () => {
      let global = new Date();
      let hours = global.getHours();
        for ( let i = 0; i < 10; i++){
            if (hours == i) hours = '0' + i;
        }
        let minutes = global.getMinutes();
        for ( let i = 0; i < 10; i++){
            if (minutes == i) minutes = '0' + i;
        }
        return hours + ':' + minutes;
    }

  send = () => {
    const itemId = this.props.navigation.getParam('itemId', itemId);

    alert(itemId)
  }

  render() {
    return (
      
          <View style={{backgroundColor:'white'}}>
            <UserSign author={this.state.author}/>
            <View style={{margin: 15}}>
                <TextInput 
                value={this.state.title}
                placeholder="Type the title"
                allowFontScaling={true}
                style={{height: 40}}
                onChangeText={(value) => this.changeTitle(value)} 
                />
                <TextInput 
                value={this.state.text}
                onChangeText={(value) => this.changeText(value)}
                multiline={true}
                placeholder="Type the post"
                />

            </View>
            <FlatList 
            data = {this.state.posts}
            renderItem={({item, index}) =>
          <View>
            <Text>Author: {item.author}</Text>
            <Text>Title:  {item.title}</Text>

            <Text>Text: {item.text}</Text>

            <Text>Date: {item.postDate}</Text>

            <Text>Time: {item.postTime}</Text>

          </View>
            }
            />

            <TouchableOpacity onPress={this.send.bind(this)}>
              <Text>Save</Text>
            </TouchableOpacity>

          </View>
        

    );
  }
}


const styles = StyleSheet.create({
})

