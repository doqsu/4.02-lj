import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Image, AsyncStorage, Text, Button } from 'react-native';
import Posts from "../components/Posts";
import pencil from '../assets/pencil.png'


class List extends Component {
  state={
    posts: [{
      author: 'username',
      title: 'title1',
      text: 'U menya bila sobaka',
      postDate: 'Chislo.Mesyac.God',
      postTime: 'Chasov:Minut'
    }],
  }

  send (index) {
    this.props.navigation.navigate('Details', {
      itemId: 86,
      otherParam: 'anything you want here',
    });
    }

  render() {

    const {navigation} = this.props;
    const posts = navigation.getParam('posts');

    return (

<View>


          <View style={{backgroundColor:'white'}}>
          <Text>{}</Text>
          <Posts posts={this.state.posts}/>   
            <View style={{alignItems: 'flex-end', margin: 15}}>
              <TouchableOpacity 
              style={{
                backgroundColor:'#123456', 
                borderRadius: 50, width:75, height: 75, 
                alignContent: 'center', 
                alignItems: 'center', 
                justifyContent: 'center'}}

              onPress={ () => {this.props.navigation.navigate('Editor')}}
              >
                <Image
                    source={pencil}
                    style={{width:40, height: 40}}
                    />
              </TouchableOpacity>
            </View>

          </View>
        </View>

    );
  }
}

export default List;

const styles = StyleSheet.create({
})

