import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator} from 'react-navigation';

import Editor from './src/screens/Editor'
import List from './src/screens/List'

const RootStack = createStackNavigator({
  List: List,
  Editor: Editor
});

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}